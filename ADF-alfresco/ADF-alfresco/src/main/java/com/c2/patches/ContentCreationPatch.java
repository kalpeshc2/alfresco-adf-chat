package com.c2.patches;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.alfresco.error.AlfrescoRuntimeException;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.admin.patch.AbstractPatch;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.security.PermissionService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.extensions.surf.util.I18NUtil;

public class ContentCreationPatch extends AbstractPatch{
	private static Log logger = LogFactory.getLog(ContentCreationPatch.class);
	   private static final String PATCH_ID = "org.alfresco.tutorial.patch.contentCreationPatch";
	   private static final String MSG_SUCCESS = PATCH_ID + ".result";
	   private static final String MSG_ERROR = PATCH_ID + ".error";
	   private static final String FOLDER_NAME = "Conversations";
	   
	   /**
	     * The Alfresco Service Registry that gives access to all public content services in Alfresco.
	     */
	   private ServiceRegistry serviceRegistry;
	   
	   public void setServiceRegistry(ServiceRegistry serviceRegistry) {
	      this.serviceRegistry = serviceRegistry;
	   }
	   
	   @Override
	   protected String applyInternal() throws Exception {
	      logger.info("Starting execution of patch: " + I18NUtil.getMessage(PATCH_ID));
	      
	      // Get the store reference for the Repository store that contains live content
	      StoreRef store = StoreRef.STORE_REF_WORKSPACE_SPACESSTORE;
	      
	      // Get root node for store
	      NodeRef rootRef = serviceRegistry.getNodeService().getRootNode(store);
	      
	      // Do the patch work
	      
	      NodeRef companyHome = getCompanyHomeNodeRef(rootRef); 
	      if(nodeService.getChildByName(companyHome, ContentModel.ASSOC_CONTAINS, FOLDER_NAME) == null) {
	    	  
	    	  createFolder(companyHome);
	      }
	      
	      logger.info("Finished execution of patch: " + I18NUtil.getMessage(PATCH_ID));
	      
	      return I18NUtil.getMessage(MSG_SUCCESS);
	   }
	   
	   private NodeRef getCompanyHomeNodeRef(NodeRef rootNodeRef) {
		    String companyHomeXPath = "/app:company_home";

		    List<NodeRef> refs = searchService.selectNodes(rootNodeRef, companyHomeXPath, null,
		      serviceRegistry.getNamespaceService(), false);
		    if (refs.size() != 1) {
		      throw new AlfrescoRuntimeException(I18NUtil.getMessage(MSG_ERROR,
		         "Company home could not be found, XPATH query " + companyHomeXPath +
		         " returned " + refs.size() + " nodes."));
		    }

		    return refs.get(0);
		}
	   
	   private void createFolder(NodeRef rootRef) {
		    
		    NodeRef parentFolderNodeRef = rootRef;

		    // Create Node
		    QName associationType = ContentModel.ASSOC_CONTAINS;
		    QName associationQName = QName.createQName(NamespaceService.CONTENT_MODEL_1_0_URI,
		            QName.createValidLocalName(FOLDER_NAME));
		    QName nodeType = ContentModel.TYPE_FOLDER;
		    Map<QName, Serializable> nodeProperties = new HashMap<QName, Serializable>();
		    nodeProperties.put(ContentModel.PROP_NAME, FOLDER_NAME);
		    ChildAssociationRef parentChildAssocRef = nodeService.createNode(
		            parentFolderNodeRef, associationType, associationQName, nodeType, nodeProperties);

		    NodeRef newFolderNodeRef = parentChildAssocRef.getChildRef();

		    // Give Contributor permission to every user
		    
		    this.serviceRegistry.getPermissionService().setPermission(newFolderNodeRef, PermissionService.ALL_AUTHORITIES, PermissionService.CONTRIBUTOR, true);
		}
}
