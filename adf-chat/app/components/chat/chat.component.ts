import { Component, OnInit, OnDestroy, Injectable, ElementRef, ViewChild } from '@angular/core';
import { AlfrescoAuthenticationService, AppConfigService } from 'ng2-alfresco-core';
import { EcmUserService } from 'ng2-alfresco-userinfo';
import { SocketService } from '../../chatservices/socket.service';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl } from '@angular/forms';


@Component({
    selector: 'chat-component',
    templateUrl: './chat.component.html',
    styleUrls: ['./chat.component.css'],
    providers: [SocketService]
})

@Injectable()
export class ChatComponent implements OnInit, OnDestroy {
    private showChat: boolean = false;
    private conversationStyle: String = 'none';
    private users: String[] = [];
    private currentuser: String;
    private logInIUser: String;
    private socketSubscribe: Subscription;
    private sendMsgDateTime: Date;
    private chatForm: FormGroup;
    private message = [];
    private ecmHost: string = '';
    private alfticket: string = '';
    private socketConnService = new SocketService();
    private socketChatService = new SocketService();
   


    constructor(private authService: AlfrescoAuthenticationService, private ecmUserService: EcmUserService,
        private appConfig: AppConfigService,private eleRef:ElementRef) {
        this.ecmHost = this.appConfig.get<string>('ecmHost');
        if (this.authService.isEcmLoggedIn) {
            this.ecmUserService.getCurrentUserInfo().subscribe(
                (response) => {
                    if (response.id !== null && response.id !== '') {
                        this.alfticket = authService.getTicketEcmBase64();
                        console.log(this.ecmHost);
                        console.log(this.alfticket);
                        // This api used for userlist
                        this.socketConnService.setConfig(this.ecmHost + '/adf-chat/chat/users', response.id);
                        this.logInIUser = response.id;

                        // This api used for chat   
                        this.socketChatService.setConfig(this.ecmHost + '/adf-chat/chat/users', '');
                    }
                }
            );
        }
    }

    ngOnInit() {

        this.chatForm = new FormGroup({
            'textmsg': new FormControl(null)
        });

        try {
            this.socketSubscribe = this.socketConnService.dataStream.subscribe((users) => {
                console.log(users.responseBody);
                let response = JSON.parse(users.responseBody);
                // console.log(response);
                if (response.users != null && response.users.length > 0) {
                    let userList = response.users;
                    console.log(userList);
                    this.users.length = 0;
                    for (let i = 0; i < userList.length; i++) {
                        if (userList[i] !== this.logInIUser) {
                            this.users.push(userList[i]);
                        }
                    }
                } else {
                    let recMsgDateTime = new Date(response.date);
                    this.message.push({ author: response.author, message: response.message, date: recMsgDateTime, to: response.to });
                }
            });
        } catch (error) {
            console.log(error);
        }
    }

    ngOnDestroy() {
        this.socketConnService.socketUnsubscribe();
        this.socketChatService.socketUnsubscribe();
        console.log('DESTROY');
    }

    sendMessage(msg: string) {
        this.sendMsgDateTime = new Date();
        this.chatForm.controls['textmsg'].setValue('');

        if (msg != null && msg.trim() !== '') {

          
            console.log(this.logInIUser + ':' + this.currentuser + ':' + msg + '' + this.sendMsgDateTime +''+this.alfticket);
            this.socketChatService.send({ author: this.logInIUser, to: this.currentuser, message: msg.trim(), date: this.sendMsgDateTime.getTime(), ticket: this.alfticket });
            this.message.push({ author: this.logInIUser, message: msg.trim(), date: this.sendMsgDateTime, to: this.currentuser });

           
            let chatul= this.eleRef.nativeElement.querySelector('.conver_wrap');
            console.log(chatul.scrollTop);
            chatul.scrollTop=chatul.scrollHeight;
            console.log( chatul.scrollTop);

        }

    }

    toggleChat() {
        this.showChat = !this.showChat;
    }

    showConversation(user: String) {
        this.chatForm.controls['textmsg'].setValue(null);
        for (let i = 0; i < this.users.length; i++) {
            console.log('length', this.users.length);
            if (user === this.users[i]) {
                this.conversationStyle = 'block';
                this.currentuser = user;
            }
        }
    }

    closeConversation() {
        if (this.conversationStyle === 'block') {
            this.conversationStyle = 'none';
        }
    }

}


