package com.contcentric.adf.chat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * @author Shubham Rajput
 *
 */
public class ResponseUsers extends Message {

	private List<String> users = new ArrayList<String>();

	public ResponseUsers() {
	}

	public ResponseUsers(Collection<String> users) {
		this.users.addAll(users);
	}

	public ResponseUsers(String author, String message, String to, long date) {
		super(author, message, to, date);
	}

	public List<String> getUsers() {
		return users;
	}

	public void setUsers(List<String> users) {
		this.users.addAll(users);
	}

}
