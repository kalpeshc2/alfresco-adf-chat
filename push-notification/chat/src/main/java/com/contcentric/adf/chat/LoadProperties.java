package com.contcentric.adf.chat;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Fetch properties value from adf-chat.properties file
 * 
 * @author Shubham Rajput
 *
 */
public class LoadProperties {

	private static final String PROP_FILE_PATH = "/adf-chat.properties";
	private static Properties prop;

	static {
		InputStream is = null;
		try {
			prop = new Properties();
			is = LoadProperties.class.getResourceAsStream(PROP_FILE_PATH);
			prop.load(is);
			is.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {

		}
	}

	public static String getPropertyValue(String key) {
		return prop.getProperty(key);
	}
}
